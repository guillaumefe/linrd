#### ⚠️ Known issues on Safari, works well with firefox, chrome

## Linrd : 
- is a way to documente any project
- uses deeped nested lists, knowed as "pipelines", as input
- serves next task(s) as output
- is provided to people who need better organization


## How it works:
- Try it:
    1. go to http://pipelinrd.herokuapp.com
    2. write yaml on the left side
    3. set first task as DONE on the right side

## Tutorial : 
    # Copy/Paste the following lines into http://linrd.ml
    - Hello! Let me explain how to use this:
        - please *click DONE*
        - you just Done a task, congratz! (*DONE this one too*)
        
    - You just DONE one task, sometimes you would prefer to : |
        - CANCEL a task:
            - the task can be safely archived
        - DELAY a task:
            - you do not have enough ressource to do the task right now
        - AWAIT a task:
            - the task is blocked by someone esle
        (*CANCEL Me*)

    - Tutorial:
        - Click **DONE** when you have understood what I've said, otherwise set it as **DELAY**ed (*click DONE or DELAY*)
        - /!\ **DONE** and **CANCELed** tasks are hidden (*click DONE or CANCEL*)
        - /!\ **DELAYed** and **AWAITed** tasks aren't hidden (*click DELAY or AWAIT*)
        - To cancel your last actions: |
            - 1. click on the text area (left side of this screen)
            - 2. push Ctrl+Z (depends on your browser)
            - 3. redo with Ctrl+Shift+Z (depends on your browser)
            (*click DONE when you have understood*)
        - If you want to involve a friend on a task **AWAIT** this task and send him the part of the pipeline that concerns him (*click AWAIT*)

    - Let me explain what we are doing:
        - Linrd is a way to craft pipelines:
            - crafting a pipeline is about detailing how to do something:
                - step by step:
                    - using nested list: |
                        - each new children clarifies the previous entry:
                        - each entry can add one or more:
                            - clarification:
                                - as new bullets point
                            - objects
                            - arrays
                        - every action is expected in order 
                        - (*click DONE*)

    - Linrd is another way to manage todos:
        - with a bit of imagination:
            - you may find new usages by yourself (*click DONE*)

    - this is a pipeline :: |
        - you can use variables:
            - example : &ref 
                - write hello:
                    - write :
                        - h
                        - e
                        - l
                        - l
                        - o
                -recipe to say world:
                    - have a small pursed mouth
                    - expel air by letting your vocal cords vibrate
                    - let more and more air pass through, opening your mouth gradually, as the sound spreads
            - *ref
            - (*click DONE*)

    - you have consumed this pipeline (*click DONE*)
            
## Another usage:
    - my shopping list  : &shopping |
        - egg 
        - bread 
        - butter 
        - milk 
        - honey 
        - vegetables 
    - my watch list  : &series |
        - twd 
        - futurama &- 
        - solar opposite
    
    - my hobby  : &code
        - documente pipelinr 
        - commit linrd.ml 
        - watch megaquizz
    
    - my professional carrier: &work
        - work on IT projects 
    
    - timetable:
        - monday:
            - *work
            - *series
        - tuesday:
            - *work
            - *code
        - wednesday:
            - *work
            - *series
        - thursday:
            - *work
            - *code
        - friday:
            - *work
            - *shopping
            - *code
        - saturday:
            - *code
            - *shopping
        - sunday:
            - *series
            - *work

## Interaction: 
    in Search bar, enter :
        - monday 
        - tuesday 
        - wednesday 
        - thursday 
        - friday 
        - saturday
        - sunday 


## More:
    - you can use the full YAML specification
    - your changes are persistent

## Version notes:
    - Linrd :
        - is not fully ready
        - is quite already usable
---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
