import React from 'react';

import { Router } from "./features/navigation/Router"

import './App.css'
  
function App() {
  return (
    <div className="App">
      Try it maybe
      <Router />
      <a href="#modal-one" class="btn btn-big">Play</a>
      <div class="modal" id="modal-one" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-header">
            <h2>Modal in CSS?</h2>
            <a href="#" class="btn-close" aria-hidden="true">×</a>
          </div>
          <div class="modal-body">
            <p>One modal example here! :D</p>
          </div>
          <div class="modal-footer">
            <a href="#" class="btn">Nice!</a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
